type: default
team: dit
group: drupal-cloud
service: acquia_perz

validate_config: true

environment_image:
  file: ".acquia/Dockerfile.ci"
  context: "."
  build_args:
    - secrets:
        - type: vault
          key: SSH_KEY
          value: GIT_SSH_KEY
          path: secret/pipeline-default/GIT_SSH_KEY
        - type: vault
          key: CLIENT_ID
          value: client_id
          path: secret/acquia_perz/client_id
        - type: vault
          key: CLIENT_SECRET
          value: client_secret
          path: secret/acquia_perz/client_secret
        - type: vault
          key: REFRESH_TOKEN
          value: refresh_token
          path: secret/acquia_perz/refresh_token
_orca_steps: &orca_steps
  steps:
    - setup_ramfs:
        - cp -ar /acquia /ramfs  &&  df -hT
    - "PHP version selection":
      - |
        cd /ramfs${CI_WORKSPACE}
        # Override the default PHP version provided by the container to PHP 8.3.
        if [ "$JENKINS_PHP_VERSION" = 8.3 ]; then

          echo 'http://dl-cdn.alpinelinux.org/alpine/v3.19/community' >> /etc/apk/repositories
          echo 'http://dl-cdn.alpinelinux.org/alpine/v3.19/main' >> /etc/apk/repositories

          # Download php83
          apk update && apk upgrade && apk add php83 php83-cli php83-common \
             php83-zip php83-gd php83-mbstring php83-tokenizer \
             php83-curl php83-bcmath php83-xml \
             php83-intl php83-sqlite3 php83-mysqli php83-dev \
             php83-gmp php83-soap php83-sockets \
             php83-phar php83-dom php83-xmlwriter php83-pdo php83-simplexml php83-ctype \
             php83-session php83-pdo_sqlite libcurl php83-pecl-apcu \
             mysql mysql-client sqlite php83-pdo_mysql php83-posix

          apk fix musl


            update-alternatives --install /usr/local/bin/php php /usr/bin/php83 82
            update-alternatives --set php /usr/bin/php83
            update-alternatives --force --all

            # Updating php-config.
            update-alternatives --install /usr/local/bin/php-config php-config /usr/bin/php-config83 82
            update-alternatives --set php-config /usr/bin/php-config83
            update-alternatives --force --all


            # Updating phpize.
            update-alternatives --install /usr/local/bin/phpize phpize /usr/bin/phpize83 82
            update-alternatives --set phpize /usr/bin/phpize83
            update-alternatives --force --all


            if [ "$ORCA_COVERAGE_ENABLE" = "TRUE" ]; then
              # Installing xdebug.
                pecl install xdebug

              # Adding Configuration
               docker-php-ext-enable xdebug
               echo "zend_extension=/usr/lib/php83/modules/xdebug.so" >> /etc/php83/php.ini
               echo xdebug.mode=coverage > /etc/php83/php.ini
            fi

            apk update && apk upgrade \
              && apk add --no-cache --virtual .build-deps --update linux-headers \
              $PHPIZE_DEPS \
              lsb-release \
              wget \
              unzip \
              gpg \
              libgd \
              bash \
              jq \
              git \
              ca-certificates

            apk del php81-sodium libsodium -r
            apk cache clean
            apk update && apk upgrade && apk add php83-sodium

            php -v

            echo 'memory_limit = 2048M' >> /etc/php83/php.ini

        fi
        # Override the default PHP version provided by the container to PHP 8.2.
        if [ "$JENKINS_PHP_VERSION" = 8.2 ]; then
          # Download php82
          apk update && apk add php82 php82-cli php82-common \
             php82-zip php82-gd php82-mbstring php82-tokenizer \
             php82-curl php82-bcmath php82-xml \
             php82-intl php82-sqlite3 php82-mysqli php82-dev \
             php82-gmp php82-soap php82-sockets \
             php82-phar php82-dom php82-xmlwriter php82-pdo php82-simplexml \
             php82-session php82-pdo_sqlite

          # Configure php
          ln -s -f /usr/bin/php82 /usr/bin/php
          ln -s -f /usr/bin/php /usr/local/bin/php

          # Configure phpize
          ln -s -f /usr/bin/phpize82 /usr/bin/phpize
          ln -s -f /usr/bin/phpize /usr/local/bin/phpize

          # Configure php-config
          ln -s -f /usr/bin/php-config82 /usr/bin/php-config
          ln -s -f /usr/bin/php-config /usr/local/bin/php-config
        fi
        # Override the default PHP version provided by the container to PHP 8.0.
        if [ "$JENKINS_PHP_VERSION" = 8.0 ]; then

        echo 'http://dl-cdn.alpinelinux.org/alpine/v3.15/community' >> /etc/apk/repositories
        echo 'http://dl-cdn.alpinelinux.org/alpine/v3.15/main' >> /etc/apk/repositories

          # Download php8.0
          apk update && apk add php8 php8-cli php8-common \
             php8-zip php8-gd php8-mbstring php8-tokenizer \
             php8-curl php8-bcmath php8-xml \
             php8-intl php8-sqlite3 php8-mysqli php8-dev \
             php8-gmp php8-soap php8-sockets \
             php8-phar php8-dom php8-xmlwriter php8-pdo php8-simplexml \
             php8-session php8-pdo_sqlite
          # Configure php
          ln -s -f /usr/bin/php8 /usr/bin/php
          ln -s -f /usr/bin/php /usr/local/bin/php

          # Configure phpize
          ln -s -f /usr/bin/phpize8 /usr/bin/phpize
          ln -s -f /usr/bin/phpize /usr/local/bin/phpize

          # Configure php-config
          ln -s -f /usr/bin/php-config8 /usr/bin/php-config
          ln -s -f /usr/bin/php-config /usr/local/bin/php-config
        fi
        # Override the default PHP version provided by the container to PHP 7.4.
        if [ "$JENKINS_PHP_VERSION" = 7.4 ]; then
          composer --version

          echo 'http://dl-cdn.alpinelinux.org/alpine/v3.15/community' >> /etc/apk/repositories
          echo 'http://dl-cdn.alpinelinux.org/alpine/v3.15/main' >> /etc/apk/repositories

          # Download php7.4
          apk update && apk add php7 php7-cli php7-common \
             php7-zip php7-gd php7-mbstring php7-tokenizer \
             php7-curl php7-bcmath php7-xml \
             php7-intl php7-sqlite3 php7-mysqli php7-dev \
             php7-gmp php7-soap php7-sockets \
             php7-phar php7-dom php7-xmlwriter php7-pdo php7-simplexml \
             php7-session php7-pdo_sqlite

          # Configure php
          ln -s -f /usr/bin/php7 /usr/bin/php
          ln -s -f /usr/bin/php /usr/local/bin/php

          # Configure phpize
          ln -s -f /usr/bin/phpize7 /usr/bin/phpize
          ln -s -f /usr/bin/phpize /usr/local/bin/phpize

          # Configure php-config
          ln -s -f /usr/bin/php-config7 /usr/bin/php-config
          ln -s -f /usr/bin/php-config /usr/local/bin/php-config
        fi

        php -v
    - "XDEBUG config":
        - |
          php_version="$(php -r 'echo PHP_VERSION;' | cut -d '.' -f 1,2)"
          if [ "$ORCA_COVERAGE_ENABLE" = "TRUE" ]; then
            echo xdebug.mode=coverage > /usr/local/etc/php/conf.d/xdebug.ini
          fi

          if [ "$ORCA_COVERAGE_ENABLE" = "FALSE" ]; then
            mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini docker-php-ext-xdebug.ini.back
            mv /usr/local/etc/php/conf.d/xdebug.ini xdebug.ini.back
          fi
    - "Chromedriver config":
       - |
         php_version="$(php -r 'echo PHP_VERSION;' | cut -d '.' -f 1,2)"
         # chrome and chromedriver downgrade to version below 115.x, we will start
         # testing with latest chromedriver once drupal core's nightwatch tests are
         # passing in them.
         # if [ "$ORCA_ENABLE_NIGHTWATCH" = "TRUE" ]; then
           if [ ${php_version} != 8.1 ]; then
             apk add 'chromium'
             apk add 'chromium-chromedriver'
           fi
         # fi
    - before_install:
        - cd /ramfs${CI_WORKSPACE}
        - |
          # If ORCA version is overridden then use that.
          if [ "$ORCA_VERSION" != "$ORCA_VERSION_OVERRIDE" ]; then
            # Remove ORCA if it is already installed and install appropriate version of
            # ORCA using the ORCA_VERSION_OVERRIDE environment variable. This allows
            # us to change the ORCA version at runtime.
            rm -rf ../orca
            composer create-project --no-dev --ignore-platform-req=php acquia/orca ../orca "$ORCA_VERSION_OVERRIDE"
          fi
          composer -d"../orca" install
          ../orca/bin/ci/before_install.sh
    - install:
        - cd /ramfs${CI_WORKSPACE}
        - ../orca/bin/ci/install.sh

    - |
      cd /ramfs${CI_WORKSPACE}
      [ -d "../orca-build" ] && composer config allow-plugins true --working-dir=../orca-build -n || exit 0
    - before_script:
        - cd /ramfs${CI_WORKSPACE}
        - ../orca/bin/ci/before_script.sh
    - script:
        - cd /ramfs${CI_WORKSPACE}
        - ../orca/bin/ci/script.sh
    - "Fix Path":
        - |
          if [ "$ORCA_COVERAGE_ENABLE" = TRUE ]; then
            cd /ramfs${CI_WORKSPACE}
            sed -i 's/\/ramfs//' /acquia/acquia_perz/clover.xml
            sed -i 's/\/ramfs//' /acquia/acquia_perz/junit.xml
          fi
_orca_job: &orca_job
  <<: *orca_steps
_orca_job_allow_failures: &orca_job_allow_failures
  - ignore_failures: true
    <<: *orca_steps
pre_build:
  # Testing Drupal 11 in php 8.3
  isolated_test_on_next_major_latest_minor_dev:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_NEXT_MAJOR_LATEST_MINOR_DEV --env JENKINS_PHP_VERSION=8.3
      <<: *orca_job
      ignore_failures: true
  integrated_test_on_next_major_latest_minor_dev:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_NEXT_MAJOR_LATEST_MINOR_DEV --env JENKINS_PHP_VERSION=8.3
      <<: *orca_job
      ignore_failures: true
  isolated_test_on_next_major_latest_minor_beta_later:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_NEXT_MAJOR_LATEST_MINOR_BETA_OR_LATER --env JENKINS_PHP_VERSION=8.3
      <<: *orca_job
      ignore_failures: true
  integrated_test_on_next_major_latest_minor_beta_later:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_NEXT_MAJOR_LATEST_MINOR_BETA_OR_LATER --env JENKINS_PHP_VERSION=8.3
      <<: *orca_job

  static_code_analysis:
    - args: --env ORCA_JOB=STATIC_CODE_ANALYSIS
      <<: *orca_job
  integrated_test_on_oldest_supported:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_OLDEST_SUPPORTED
      <<: *orca_job
  integrated_test_on_latest_lts:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_LATEST_LTS
      <<: *orca_job
  integrated_test_on_latest_lts_php82:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_LATEST_LTS  --env JENKINS_PHP_VERSION=8.2
      <<: *orca_job
  integrated_test_on_prev_minor:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_PREVIOUS_MINOR
      <<: *orca_job
  # integrated_test_from_prev_minor:
  #   - args: --env ORCA_JOB=INTEGRATED_UPGRADE_TEST_FROM_PREVIOUS_MINOR
  #     <<: *orca_job
  isolated_test_on_current:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_CURRENT --env ORCA_COVERAGE_ENABLE=TRUE --env ORCA_COVERAGE_CLOVER=/acquia/acquia_perz/clover.xml --env ORCA_JUNIT_LOG=/acquia/acquia_perz/junit.xml
      ca_data: /acquia
      <<: *orca_job
  integrated_test_on_current:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_CURRENT
      <<: *orca_job
  # integrated_test_to_next_minor:
  #   - args: --env ORCA_JOB=INTEGRATED_UPGRADE_TEST_TO_NEXT_MINOR
  #     <<: *orca_job
  # integrated_test_to_next_minor_dev:
  #   - args: --env ORCA_JOB=INTEGRATED_UPGRADE_TEST_TO_NEXT_MINOR_DEV
  #     <<: *orca_job
  isolated_test_on_current_dev:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_CURRENT_DEV
      <<: *orca_job
  integrated_test_on_current_dev:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_CURRENT_DEV
      <<: *orca_job
  strict_deprecated_code_scan:
    - args: --env ORCA_JOB=STRICT_DEPRECATED_CODE_SCAN
      <<: *orca_job
  isolated_test_on_next_minor:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_NEXT_MINOR
      <<: *orca_job
  isolated_test_on_next_minor_dev:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_NEXT_MINOR_DEV
      <<: *orca_job
  isolated_test_on_next_minor_dev_php82:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_NEXT_MINOR_DEV --env JENKINS_PHP_VERSION=8.2
      <<: *orca_job
  integrated_test_on_next_minor:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_NEXT_MINOR
      <<: *orca_job
  integrated_test_on_next_minor_dev:
    - args: --env ORCA_JOB=INTEGRATED_TEST_ON_NEXT_MINOR_DEV
      <<: *orca_job
  isolated_test_on_current_php82:
    - args: --env ORCA_JOB=ISOLATED_TEST_ON_CURRENT --env JENKINS_PHP_VERSION=8.2
      <<: *orca_job
  deprecated_code_scan_with_contrib:
    - args: --env ORCA_JOB=DEPRECATED_CODE_SCAN_W_CONTRIB
      <<: *orca_job
  loose_deprecated_code_scan:
    - args: --env ORCA_JOB=LOOSE_DEPRECATED_CODE_SCAN
      <<: *orca_job

  # isolated_upgrade_test_to_next_major_dev:
  #   - args: --env ORCA_JOB=ISOLATED_UPGRADE_TEST_TO_NEXT_MAJOR_DEV
  #     <<: *orca_job
  # isolated_upgrade_test_to_next_major_beta_later:
  #   - args: --env ORCA_JOB=ISOLATED_UPGRADE_TEST_TO_NEXT_MAJOR_BETA_OR_LATER
  #     <<: *orca_job


  security_composition_analysis:
    required: false

  code_analysis:
    required: true
    project_key: acquia.drupal-cloud.dit:acquia_perz
    quality_gate:
      wait_for_quality_gate: false
      max_time_limit: 2
      abort_pipeline: false

#notify:
#  channel: drupal-integration-eng
#  on_success: change
#  on_failure: always
