<?php

/**
 * @file
 * Acquia Perz - module install file.
 */

use Drupal\acquia_perz\PerzHelper;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_install().
 */
function acquia_perz_install(): void {

  PerzHelper::createSiteHash();
  PerzHelper::migrateSiteId();

  // Ensure acquia_perz hooks are invoked after acquia_lift.
  module_set_weight('acquia_perz', 10);
}

/**
 * Implements hook_uninstall().
 */
function acquia_perz_uninstall(): void {

  // Delete entity config.
  $perz_entity_config = \Drupal::configFactory()->getEditable('acquia_perz.entity_config');
  $perz_entity_config->delete();

  // Delete perz cis settings.
  $perz_api_settings = \Drupal::configFactory()->getEditable('acquia_connector.settings');
  $perz_api_settings->clear('third_party_settings.acquia_perz');
  $perz_api_settings->save();

  // Remove perz state config.
  \Drupal::state()->delete('acquia_perz.environment');
  \Drupal::state()->delete('acquia_perz.site_hash');
}

/**
 * Implements hook_requirements().
 */
function acquia_perz_requirements($phase) {
  $requirements = [];

  if ($phase !== 'runtime') {
    return $requirements;
  }

  $config_error_messages = PerzHelper::getConfigErrorMessages();

  if (!empty($config_error_messages)) {
    $requirements['acquia_perz_configuration_status'] = [
      'title' => t('Acquia Personalization Configuration Status'),
      'severity' => REQUIREMENT_ERROR,
      'description' => Markup::create(implode('<br>', $config_error_messages)),
    ];
  }

  return $requirements;
}
