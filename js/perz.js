/**
 * @file
 * Attach behaviors to the HTML replaced by Personalization.
 */

(function (Drupal, settings) {
  window.addEventListener('acquiaLiftContentAvailable' , (event) => {
    const slotElement = document.querySelector(`[data-lift-slot="${event.detail.decision_slot_id}"]`);
    Drupal.attachBehaviors(null, settings);
    // Additional Fix for Experience builder
    if (typeof window.AcquiaLift !== 'undefined') {
      if (typeof window.AcquiaLift.executionContext !== 'undefined' &&
          (window.AcquiaLift.executionContext === "experience-builder-iframe" ||
              window.AcquiaLift.executionContext === "experience-builder-session")) {
        const config = { attributes: false, childList: true, subtree: true };
        const callback = (mutationList) => {
          for (const mutation of mutationList) {
            if (mutation.type === "childList") {
              Drupal.attachBehaviors(slotElement, settings);
            }
          }
        };
        const observer = new MutationObserver(callback);
        observer.observe(slotElement, config);
      }
    }
  })
})(Drupal, drupalSettings);
