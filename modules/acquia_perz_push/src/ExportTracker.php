<?php

namespace Drupal\acquia_perz_push;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The publisher tracker table class.
 */
class ExportTracker {

  const DELETED = 'deleted';

  const DELETE_TIMEOUT = 'delete_timeout';

  const EXPORTED = 'exported';

  const EXPORT_TIMEOUT = 'export_timeout';

  const FAILED = 'failed';

  /**
   * The name of the tracking table.
   */
  const EXPORT_TRACKING_TABLE = 'acquia_perz_push_export_tracking';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * PublisherTracker constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets the tracking record for a given uuid.
   *
   * @param string $uuid
   *   The entity uuid.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @return mixed
   *   Database statement
   */
  public function get(string $uuid, string $langcode = ''): mixed {
    $query = $this->database->select(self::EXPORT_TRACKING_TABLE, 't')
      ->fields('t', ['entity_uuid']);
    $query->condition('entity_uuid', $uuid);
    if (!empty($langcode)) {
      $query->condition('langcode', $langcode);
    }
    return $query->execute()->fetchObject();
  }

  /**
   * Track entity and its languages.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id.
   * @param string $langcode
   *   The langcode of the tracking entity.
   * @param string $action
   *   The tracking action.
   *
   * @throws \Exception
   */
  public function trackEntity(string $entity_type_id, int $entity_id, string $langcode = 'all', string $action = 'export'): void {
    $this->clear(
      $entity_type_id,
      $entity_id
    );
    $entity = $this
      ->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);
    $entity_uuid = $entity->uuid();
    if ($langcode === 'all') {
      if ($entity instanceof ContentEntityBase) {
        foreach ($entity->getTranslationLanguages() as $language) {
          $this->{$action}(
            $entity_type_id,
            $entity_id,
            $entity_uuid,
            $language->getId()
          );
        }
      }
    }
    else {
      $this->{$action}(
        $entity_type_id,
        $entity_id,
        $entity_uuid,
        $langcode
      );
    }
  }

  /**
   * Clear tracking for an entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id.
   *
   * @throws \Exception
   */
  public function clear(string $entity_type_id, int $entity_id): void {
    $query = $this->database->delete(self::EXPORT_TRACKING_TABLE);
    $query->condition('entity_type', $entity_type_id);
    $query->condition('entity_id', $entity_id);
    $query->execute();
  }

  /**
   * Add tracking for an entity in a self::EXPORTED state.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id.
   * @param string $entity_uuid
   *   The entity uuid.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @throws \Exception
   */
  public function export(string $entity_type_id, int $entity_id, string $entity_uuid, string $langcode): void {
    $this->insertOrUpdate($entity_type_id, $entity_id, $entity_uuid, self::EXPORTED, $langcode);
  }

  /**
   * Add tracking for an entity in a self::EXPORT_TIMEOUT state.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id of the entity.
   * @param string $entity_uuid
   *   The entity uuid of the entity.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @throws \Exception
   */
  public function exportTimeout(string $entity_type_id, int $entity_id, string $entity_uuid, string $langcode): void {
    $this->insertOrUpdate($entity_type_id, $entity_id, $entity_uuid, self::EXPORT_TIMEOUT, $langcode);
  }

  /**
   * Delete tracking for an entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id of the entity.
   * @param string $entity_uuid
   *   The entity uuid of the entity.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   *   Database statement
   *
   * @throws \Exception
   */
  public function delete(string $entity_type_id, int $entity_id, string $entity_uuid, string $langcode = ''): StatementInterface|int|null {
    return $this->insertOrUpdate($entity_type_id, $entity_id, $entity_uuid, self::DELETED, $langcode);
  }

  /**
   * Delete timeout tracking for an entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id of the entity.
   * @param string $entity_uuid
   *   The entity uuid of the entity.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   *   Database statement
   *
   * @throws \Exception
   */
  public function deleteTimeout(string $entity_type_id, int $entity_id, string $entity_uuid, string $langcode = ''): StatementInterface|int|null {
    return $this->insertOrUpdate($entity_type_id, $entity_id, $entity_uuid, self::DELETE_TIMEOUT, $langcode);
  }

  /**
   * Determines if an entity will be inserted or updated with a status.
   *
   * @param string $entity_type_id
   *   The entity type id of the entity.
   * @param int $entity_id
   *   The entity id of the entity.
   * @param string $entity_uuid
   *   The entity uuid of the entity.
   * @param string $status
   *   The status of the tracking.
   * @param string $langcode
   *   The langcode of the tracking entity.
   *
   * @return mixed
   *   Database statement.
   *
   * @throws \Exception
   */
  protected function insertOrUpdate(string $entity_type_id, int $entity_id, string $entity_uuid, string $status, string $langcode = ''): mixed {
    $modified = date('c');
    $results = $this->get($entity_uuid, $langcode);
    if ($results) {
      $values = ['modified' => $modified, 'status' => $status];
      $query = $this->database->update(self::EXPORT_TRACKING_TABLE)
        ->fields($values);
      $query->condition('entity_uuid', $entity_uuid);
      if (!empty($langcode)) {
        $query->condition('langcode', $langcode);
      }
      return $query->execute();
    }
    $values = [
      'entity_type' => $entity_type_id,
      'entity_id' => $entity_id,
      'entity_uuid' => $entity_uuid,
      'status' => $status,
      'langcode' => $langcode,
      'modified' => $modified,
    ];
    return $this->database->insert(self::EXPORT_TRACKING_TABLE)
      ->fields($values)
      ->execute();
  }

  /**
   * Checks if a particular entity uuid is tracked.
   *
   * @param string $uuid
   *   The uuid of an entity.
   * @param string $langcode
   *   The langcode of an entity.
   *
   * @return bool
   *   Whether or not the entity is tracked in the subscriber tables.
   */
  public function isTracked(string $uuid, string $langcode): bool {
    $query = $this->database->select(self::EXPORT_TRACKING_TABLE, 't');
    $query->fields('t', ['entity_type', 'entity_id']);
    $query->condition('entity_uuid', $uuid);
    $query->condition('langcode', $langcode);

    return (bool) $query->execute()->fetchObject();
  }

}
